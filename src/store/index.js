import Vue from 'vue';
import Vuex from 'vuex';
import photos from './modules/photos';
import users from './modules/users';
import albums from './modules/albums';

Vue.use(Vuex);

export default new Vuex.Store({
    strict: true,
    modules: {
       photos, users, albums
    }
});