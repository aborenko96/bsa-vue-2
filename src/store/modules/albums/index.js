const state = {
    albums: [

    ]
};

const getters = {
    album: state => id => {
        return state.albums[state.albums.findIndex(album => album.id === id)];
    },
};

const mutations = {
    ADD_ALBUM(state, album) {
        state.albums.push({
            id: album.id,
            user: album.userId,
            title: album.title
        });
    }
}

const actions = {
    addAlbum({ state, commit, rootState }, data) {
        return new Promise(resolve => {
            commit('ADD_ALBUM', data);
            resolve();
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};