import Vue from "vue";

let lastId = 0;

const state = {
    users: []
};

const getters = {
    getUser: state => id => {
        return state.users.find(user => user.id == id);
    },
    sortedByName: state => {
        return state.users.sort((a, b) => a.name < b.name);
    }
};

const mutations = {
    ADD_USER(state, user) {
        lastId++;

        state.users.push({
            id: lastId,
            name: user.name,
            email: user.email,
            avatar: user.avatar
        });
    },

    DELETE_USER(state, userId) {
        const ind = state.users.findIndex(user => user.id === userId);

        if (ind !== -1) {
            state.users.splice(ind, 1);
        }
    },

    EDIT_USER(state, {userId, data}) {
        const ind = state.users.findIndex(user => user.id === userId);

        if (ind !== -1) {
            const updatedUser = {
                id: userId,
                name: data.name,
                age: data.age
            };

            Vue.set(state.users, ind, updatedUser);
        }
    }
};

const actions = {
    // first param is context object
    addUser({state, commit, rootState}, data) {
        return new Promise(resolve => {
            commit('ADD_USER', data);
            resolve();
        });
    },

    deleteUser({commit}, userId) {
        console.log('here');
        return new Promise(resolve => {
            commit('DELETE_USER', userId);
            resolve();
        });
    },

    editUser({commit}, data) {
        return new Promise(resolve => {
            commit('EDIT_USER', data);
            resolve();
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};