const state = {
    photos: [

    ]
};

const getters = {
    photo: state => id => {
        return state.photos[state.photos.findIndex(photo => photo.id === id)];
    },

    getRandomPhoto: state => rand => {
        return state.photos[Math.floor(rand*state.photos.length)];
    }
};

const mutations = {
    ADD_PHOTO(state, photo) {
        state.photos.push({
            id: photo.id,
            album: photo.albumId,
            title: photo.title,
            url: photo.url
        });
    }
}

const actions = {
    addPhoto({ state, commit, rootState }, data) {
        return new Promise(resolve => {
            commit('ADD_PHOTO', data);
            resolve();
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};