import Vue from 'vue';
import Router from 'vue-router';
import UserList from "./components/UserList.vue";
import Users from "./components/Users.vue";

Vue.use(Router);

export default new Router({
    base: "/",
    mode: "history",
    routes: [
        {
            path: "/",
            redirect: "/users"
        },
        {
            path: "/users",
            component: UserList
        },

        {
            path: "/user/:id",
            component: Users
        }
    ]
});
